#Rapport

```java 
#MyBenchmark.java
...
    @Benchmark @OutputTimeUnit(TimeUnit.SECONDS)
    public List<Integer> testCombSort(MyState state) {
        List<Integer> listCp = state.getList();
        Sort comb = new CombSort();

        return comb.sort(listCp);
    }
...
```
JMH récupere l'objet state instancié pour les benchmark.
Cela permet de faire une séparation des classes de benchmark et de gestion de données.
Ici nous parametrons les sorties du benchmark avec le temps d'execution en secondes et la vitesse moyenne d'execution, en opération par unité de temps.


```java
#MyState.java
@State(Scope.Benchmark)
public class MyState {
    private  static final int CAPACITY = 1_000;
    private  static final long SEED = 1_000;

    private List<Integer> list;

...
    }
```
Ici nous avons une classe `@State` contenant les données pour nos benchmark, tout les benchmark utilise la même instance avec `@State(Scope.Benchmark)`.

```java
#MyState.java
...
    @Setup(Level.Invocation)
    public void setup() {
        list = init();
    }
...
```
`@Setup(Level.Invocation)` nous permet de réinitialiser la liste de l'instance de MyState partagée par tout les benchmark, et est appelé a chaque invocation du benchmark qui à besoin de MyState.






