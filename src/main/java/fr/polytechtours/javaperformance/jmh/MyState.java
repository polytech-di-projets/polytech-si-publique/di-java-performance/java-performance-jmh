package fr.polytechtours.javaperformance.jmh;

import org.openjdk.jmh.annotations.Level;
import org.openjdk.jmh.annotations.Scope;
import org.openjdk.jmh.annotations.Setup;
import org.openjdk.jmh.annotations.State;

import java.util.*;


@State(Scope.Benchmark)
public class MyState {

    private  static final int CAPACITY = 1_000;
    private  static final long SEED = 1_000;

    private List<Integer> list;

    private List<Integer> init() {
        List<Integer> listReturn = new ArrayList<>(CAPACITY);
        Random random = new Random(SEED);
        for (int i = 0; i <  CAPACITY; i++) {
            listReturn.add( random.nextInt());
        }
        return listReturn;

    }

    public List<Integer> getList() {
        return list;
    }

    @Setup(Level.Invocation)
    public void setup() {
        list = init();
    }

}
